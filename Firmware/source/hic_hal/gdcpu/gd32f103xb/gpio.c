/**
 * @file    gpio.c
 * @brief
 *
 * DAPLink Interface Firmware
 * Copyright (c) 2009-2016, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stm32f10x.h"
#include "DAP_config.h"
#include "gpio.h"
#include "target_reset.h"
#include "daplink.h"
#include "util.h"


static void busy_wait(uint32_t cycles)
{
    volatile uint32_t i;
    i = cycles;

    while (i > 0) {
        i--;
    }
}


void gpio_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    // enable clock to ports
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |
                           RCC_APB2Periph_GPIOB | 
                           RCC_APB2Periph_GPIOC |
                           RCC_APB2Periph_GPIOD |
                           RCC_APB2Periph_AFIO, ENABLE); 
    
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable , ENABLE);
    

    USB_CONNECT_PORT_ENABLE();
    USB_CONNECT_OFF();
    GPIO_InitStructure.GPIO_Pin = USB_CONNECT_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(USB_CONNECT_PORT, &GPIO_InitStructure);
    
    // RUNNING LED
    GPIO_WriteBit(RUNNING_LED_PORT, RUNNING_LED_PIN, Bit_SET);
    GPIO_InitStructure.GPIO_Pin = RUNNING_LED_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(RUNNING_LED_PORT, &GPIO_InitStructure);

    // CONNECTEDLED
    GPIO_WriteBit(CONNECTED_LED_PORT, CONNECTED_LED_PIN, Bit_SET);
    GPIO_InitStructure.GPIO_Pin = CONNECTED_LED_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(CONNECTED_LED_PORT, &GPIO_InitStructure);
    
    // HID LED
    GPIO_WriteBit(PIN_HID_LED_PORT, PIN_HID_LED_PIN , Bit_SET);
    GPIO_InitStructure.GPIO_Pin = PIN_HID_LED_PIN ;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(PIN_HID_LED_PORT, &GPIO_InitStructure);

    //CDC LED
    GPIO_WriteBit(PIN_CDC_LED_PORT, PIN_CDC_LED_PIN, Bit_SET);
    GPIO_InitStructure.GPIO_Pin = PIN_CDC_LED_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(PIN_CDC_LED_PORT, &GPIO_InitStructure);

    //MSC LED
    GPIO_WriteBit(PIN_MSC_LED_PORT, PIN_MSC_LED_PIN, Bit_SET);
    GPIO_InitStructure.GPIO_Pin = PIN_MSC_LED_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(PIN_MSC_LED_PORT, &GPIO_InitStructure);

    // reset button configured as gpio open drain output with a pullup
    GPIO_WriteBit(nRESET_PIN_PORT, nRESET_PIN, Bit_SET);
    GPIO_InitStructure.GPIO_Pin = nRESET_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    GPIO_Init(nRESET_PIN_PORT, &GPIO_InitStructure);
    GPIO_SetBits(nRESET_PIN_PORT, nRESET_PIN);
    
    // reset BL ISP
    GPIO_WriteBit(nRESET_ISP_PIN_PORT, nRESET_ISP_PIN, Bit_SET);
    GPIO_InitStructure.GPIO_Pin = nRESET_ISP_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(nRESET_ISP_PIN_PORT, &GPIO_InitStructure);
    //GPIO_SetBits(nRESET_ISP_PIN_PORT, nRESET_ISP_PIN);
    
    
    GPIO_WriteBit(nRESET_BL_PIN_PORT, nRESET_BL_PIN, Bit_SET);
    GPIO_InitStructure.GPIO_Pin = nRESET_BL_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(nRESET_BL_PIN_PORT, &GPIO_InitStructure);
    //GPIO_SetBits(nRESET_BL_PIN_PORT, nRESET_BL_PIN);
    
    //POWER
    GPIO_ResetBits(PIN_POWER_PORT, PIN_POWER_PIN);
    GPIO_InitStructure.GPIO_Pin = PIN_POWER_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(PIN_POWER_PORT, &GPIO_InitStructure);
    GPIO_SetBits(PIN_POWER_PORT, PIN_POWER_PIN);
    // Let the voltage rails stabilize.  This is especailly important
    // during software resets, since the target's 3.3v rail can take
    // 20-50ms to drain.  During this time the target could be driving
    // the reset pin low, causing the bootloader to think the reset
    // button is pressed.
    // Note: With optimization set to -O2 the value 1000000 delays for ~85ms
    busy_wait(1000000);
}

void gpio_set_hid_led(gpio_led_state_t state)
{
    // LED is active low
    //state  ? GPIO_ResetBits(PIN_HID_LED_PORT, PIN_HID_LED_PIN) : GPIO_SetBits(PIN_HID_LED_PORT, PIN_HID_LED_PIN);
    GPIO_WriteBit(PIN_HID_LED_PORT, PIN_HID_LED_PIN, state ? Bit_RESET : Bit_SET);
}

void gpio_set_cdc_led(gpio_led_state_t state)
{
    // LED is active low
   // state  ? GPIO_ResetBits(PIN_CDC_LED_PORT, PIN_CDC_LED_PIN) : GPIO_SetBits(PIN_CDC_LED_PORT, PIN_CDC_LED_PIN);
   GPIO_WriteBit(PIN_CDC_LED_PORT, PIN_CDC_LED_PIN, state ? Bit_RESET : Bit_SET);
}

void gpio_set_msc_led(gpio_led_state_t state)
{
    // LED is active low
    //state  ? GPIO_SetBits(PIN_MSC_LED_PORT, PIN_MSC_LED_PIN) : GPIO_ResetBits(PIN_MSC_LED_PORT, PIN_MSC_LED_PIN);
		GPIO_WriteBit(PIN_MSC_LED_PORT, PIN_MSC_LED_PIN, state ? Bit_RESET : Bit_SET);
}

uint8_t gpio_get_reset_btn_no_fwrd(void)
{
    return GPIO_ReadOutputDataBit(nRESET_PIN_PORT, nRESET_PIN) ? 0 : 1;
}

uint8_t gpio_get_reset_btn_fwrd(void)
{
   return GPIO_ReadInputDataBit(nRESET_BL_PIN_PORT, nRESET_BL_PIN) ? 0 : 1;
}


uint8_t GPIOGetButtonState(void)
{
    return 0;
}

void target_forward_reset(bool assert_reset)
{
    // Do nothing - reset is forwarded in gpio_get_sw_reset
}

void gpio_set_board_power(bool powerEnabled)
{
}
