/**
 * @file    uart.c
 * @brief
 *
 * DAPLink Interface Firmware
 * Copyright (c) 2009-2016, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "string.h"

#include "stm32f10x.h"
#include "uart.h"
#include "gpio.h"
#include "util.h"
#include "circ_buf.h"
#include "IO_Config.h"

// For usart
#define CDC_UART                     USART2
#define CDC_UART_ENABLE()            RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE)
#define CDC_UART_DISABLE()           RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, DISABLE)
#define CDC_UART_IRQn                USART2_IRQn
#define CDC_UART_IRQn_Handler        USART2_IRQHandler

#define UART_PINS_PORT_ENABLE()      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE)
#define UART_PINS_PORT_DISABLE()     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, DISABLE)

#define UART_TX_PORT                 GPIOA
#define UART_TX_PIN                  GPIO_Pin_2

#define UART_RX_PORT                 GPIOA
#define UART_RX_PIN                  GPIO_Pin_3


#define RX_OVRF_MSG         "<DAPLink:Overflow>\n"
#define RX_OVRF_MSG_SIZE    (sizeof(RX_OVRF_MSG) - 1)
#define BUFFER_SIZE         (512)

circ_buf_t write_buffer;
uint8_t write_buffer_data[BUFFER_SIZE];
circ_buf_t read_buffer;
uint8_t read_buffer_data[BUFFER_SIZE];

static UART_Configuration configuration = {
    .Baudrate = 9600,
    .DataBits = UART_DATA_BITS_8,
    .Parity = UART_PARITY_NONE,
    .StopBits = UART_STOP_BITS_1,
    .FlowControl = UART_FLOW_CONTROL_NONE,
};

extern uint32_t SystemCoreClock;



static void clear_buffers(void)
{
    circ_buf_init(&write_buffer, write_buffer_data, sizeof(write_buffer_data));
    circ_buf_init(&read_buffer, read_buffer_data, sizeof(read_buffer_data));
}

int32_t uart_initialize(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    CDC_UART->CR1 &= ~(USART_IT_TXE | USART_IT_RXNE);
    clear_buffers();

    CDC_UART_ENABLE();
    UART_PINS_PORT_ENABLE();

    //TX pin
    GPIO_InitStructure.GPIO_Pin = UART_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(UART_TX_PORT, &GPIO_InitStructure);

    //RX pin
	GPIO_InitStructure.GPIO_Pin = UART_RX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(UART_RX_PORT, &GPIO_InitStructure);
    

    NVIC_EnableIRQ(CDC_UART_IRQn);

    return 1;
}

int32_t uart_uninitialize(void)
{
    CDC_UART->CR1 &= ~(USART_IT_TXE | USART_IT_RXNE);
    clear_buffers();
    return 1;
}

int32_t uart_reset(void)
{
    const uint32_t cr1 = CDC_UART->CR1;
    CDC_UART->CR1 = cr1 & ~(USART_IT_TXE | USART_IT_RXNE);
    clear_buffers();
    CDC_UART->CR1 = cr1 & ~USART_IT_TXE;
    return 1;
}

int32_t uart_set_configuration(UART_Configuration *config)
{
    USART_InitTypeDef USART_InitStructure;
    
    USART_Cmd(CDC_UART, DISABLE);
    // parity
    configuration.Parity = config->Parity;
    switch(config->Parity)
    {
		case UART_PARITY_NONE:	USART_InitStructure.USART_Parity = USART_Parity_No;		break;
		case UART_PARITY_EVEN:	USART_InitStructure.USART_Parity = USART_Parity_Even;	break;
		case UART_PARITY_ODD:	USART_InitStructure.USART_Parity= USART_Parity_Odd;	break;
		default: {
            USART_InitStructure.USART_Parity = USART_Parity_No;
            configuration.Parity = UART_PARITY_NONE;
        };
        break;
    }

	/* Stop bits */
    configuration.StopBits = config->StopBits;
	switch (config->StopBits)
	{
		case UART_STOP_BITS_1:	USART_InitStructure.USART_StopBits = USART_StopBits_1;		break;
		case UART_STOP_BITS_2:	USART_InitStructure.USART_StopBits = USART_StopBits_2;		break;
		case UART_STOP_BITS_1_5: USART_InitStructure.USART_StopBits = USART_StopBits_1_5;	break;
		default: {
            USART_InitStructure.USART_StopBits = USART_StopBits_1;
            configuration.StopBits = UART_STOP_BITS_1;
        };
        break;
	}
    //Only 8 bit support
    configuration.DataBits =  UART_DATA_BITS_8;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;

    //flow control
   configuration.FlowControl = config->FlowControl;
	switch (config->FlowControl)
	{
        case UART_FLOW_CONTROL_NONE: {
            USART_InitStructure.USART_HardwareFlowControl  = USART_HardwareFlowControl_None;
        }break;
        
        case UART_FLOW_CONTROL_RTS_CTS:{
            USART_InitStructure.USART_HardwareFlowControl  = USART_HardwareFlowControl_RTS_CTS;
        }break;
        
        default:{
            USART_InitStructure.USART_HardwareFlowControl  = USART_HardwareFlowControl_None;
            configuration.FlowControl = UART_FLOW_CONTROL_NONE;
        }break;
	}
    
    // Specified baudrate
    configuration.Baudrate = config->Baudrate;
    USART_InitStructure.USART_BaudRate= config->Baudrate;

    // TX and RX
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    
    // Disable uart and tx/rx interrupt
    
    CDC_UART->CR1 &= ~(USART_IT_TXE | USART_IT_RXNE);

    clear_buffers();

    USART_DeInit(CDC_UART);
      
    //USART_ClearFlag(CDC_UART, USART_FLAG_TC);
    
    USART_Init(CDC_UART, &USART_InitStructure);
    USART_ITConfig(CDC_UART, USART_IT_RXNE, ENABLE);
    USART_Cmd(CDC_UART, ENABLE);
    return 1;
}

int32_t uart_get_configuration(UART_Configuration *config)
{
    config->Baudrate = configuration.Baudrate;
    config->DataBits = configuration.DataBits;
    config->Parity   = configuration.Parity;
    config->StopBits = configuration.StopBits;
    config->FlowControl = UART_FLOW_CONTROL_NONE;

    return 1;
}

int32_t uart_write_free(void)
{
    return circ_buf_count_free(&write_buffer);
}

int32_t uart_write_data(uint8_t *data, uint16_t size)
{
    uint32_t cnt = circ_buf_write(&write_buffer, data, size);
    CDC_UART->CR1 |= USART_IT_TXE;

    return cnt;
}

int32_t uart_read_data(uint8_t *data, uint16_t size)
{
    return circ_buf_read(&read_buffer, data, size);
}

void CDC_UART_IRQn_Handler(void)
{
    const uint32_t sr = CDC_UART->SR;

    if (sr & USART_SR_RXNE) {
        uint8_t dat = CDC_UART->DR;
        uint32_t free = circ_buf_count_free(&read_buffer);
        if (free > RX_OVRF_MSG_SIZE) {
            circ_buf_push(&read_buffer, dat);
        } else if (RX_OVRF_MSG_SIZE == free) {
            circ_buf_write(&read_buffer, (uint8_t*)RX_OVRF_MSG, RX_OVRF_MSG_SIZE);
        } else {
            // Drop character
        }
    }

    if (sr & USART_SR_TXE) {
        if (circ_buf_count_used(&write_buffer) > 0) {
            CDC_UART->DR = circ_buf_pop(&write_buffer);
        } else {
            CDC_UART->CR1 &= ~USART_IT_TXE;
        }
    }
}
